import { createStackNavigator, createAppContainer } from 'react-navigation';
import Search from '../Components/search';
import FilmDetail from '../Components/filmDetail';

const SearchStackNavigator = createStackNavigator({
    Search: {
      screen: Search,
      navigationOptions: {
        title: 'Rechercher'
      }
    },

    FilmDetail: {
      screen: FilmDetail
    }
})

  export default createAppContainer(SearchStackNavigator)